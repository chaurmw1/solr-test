﻿using SolrNet.Attributes;

namespace SharedEntity
{
    public class Page
    {
        [SolrUniqueKey("id")]
        public string Id { get; set; }

        [SolrField("pagetitle")]
        public string PageTitle { get; set; }

        [SolrField("pagedescription")]
        public string PageDescription { get; set; }

        [SolrField("content")]
        public string Content { get; set; }
    }
}
