﻿using System;
using System.Collections.Generic;
using SharedEntity;
using SolrNet;

namespace SearchLibrary
{
    public class ResetSolrIndex
    {
        public void ResetIndex(string solrUrl, List<Page> pages)
        {
            var solrInstance = Connection.GetSolrInstance();

            solrInstance.Delete(SolrQuery.All);
            solrInstance.AddRange(pages);
            solrInstance.Commit();
        }
    }
}
