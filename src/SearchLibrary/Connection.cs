﻿using CommonServiceLocator;
using SharedEntity;
using SolrNet;

namespace SearchLibrary
{
    public static class Connection
    {

        public static void InitializeSolrConnection(string solrUrl)
        {
            Startup.Init<Page>(solrUrl);
        }

        public static ISolrOperations<Page> GetSolrInstance()
        {
            return ServiceLocator.Current.GetInstance<ISolrOperations<Page>>();
        }

    }
}
