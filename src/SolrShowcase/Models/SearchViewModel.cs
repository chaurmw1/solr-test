﻿using System.Collections.Generic;
using Umbraco.Web;

namespace SolrShowcase.Models
{
    public class SearchViewModel : Umbraco.Web.Models.RenderModel
    {
        public SearchViewModel()
          : base(UmbracoContext.Current.PublishedContentRequest.PublishedContent, UmbracoContext.Current.PublishedContentRequest.Culture)
        {
        }
        public List<string> Results { get; set; }
    }
}