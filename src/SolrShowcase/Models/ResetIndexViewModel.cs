﻿using System.Collections.Generic;
using Umbraco.Web;

namespace SolrShowcase.Models
{
    public class ResetIndexViewModel : Umbraco.Web.Models.RenderModel
    {
        public ResetIndexViewModel()
          : base(UmbracoContext.Current.PublishedContentRequest.PublishedContent, UmbracoContext.Current.PublishedContentRequest.Culture)
        {
        }
        public string Message { get; set; }
    }
}