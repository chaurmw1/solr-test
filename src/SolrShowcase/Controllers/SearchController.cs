﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using SolrShowcase.Models;
using umbraco;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace SolrShowcase.Controllers
{
    public class SearchController : SurfaceRenderMvcController
    {

        public SearchController(): this(UmbracoContext.Current)
        {
        }

        public SearchController(UmbracoContext umbracoContext): base(umbracoContext)
        {
        }

        [HttpGet]
        public ActionResult Search(string term = "")
        {

            var searchViewModel = new SearchViewModel
            {
                Results = new List<string> { term}
            };

            return CurrentTemplate(searchViewModel);
        }

        [HttpPost]
        public ActionResult Index(string term = "")
        {
            var redirectUrl = $"/search?term={term}";
            return Redirect(redirectUrl);
        }

    }
}