﻿using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace SolrShowcase.Controllers
{
    public abstract class SurfaceRenderMvcController : SurfaceController, IRenderMvcController
    {
        protected SurfaceRenderMvcController() : this(UmbracoContext.Current)
        {
        }

        protected SurfaceRenderMvcController(UmbracoContext umbracoContext) : base(umbracoContext)
        {
        }

        public virtual ActionResult Index(RenderModel model)
        {
            return CurrentTemplate(model);
        }


        protected ActionResult CurrentTemplate<T>(T model)
        {
            var template = ControllerContext.RouteData.Values["action"].ToString();
            return View(template, model);
        }
    }
}