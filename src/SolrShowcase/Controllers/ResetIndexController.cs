﻿using SearchLibrary;
using System.Collections.Generic;
using System.Linq;
using System.Web.Configuration;
using System.Web.Mvc;
using SharedEntity;
using SolrShowcase.Models;
using Umbraco.Core.Models;
using Umbraco.Web;


namespace SolrShowcase.Controllers
{
    public class ResetIndexController : SurfaceRenderMvcController
    {
        public ResetIndexController() : this(UmbracoContext.Current)
        {
        }

        public ResetIndexController(UmbracoContext umbracoContext) : base(umbracoContext)
        {
        }

        public ActionResult ResetIndex()
        {
            return CurrentTemplate(new ResetIndexViewModel());
        }

        [HttpPost]
        public ActionResult Index()
        {
            var pages = new List<Page>();
            var rootNode = Umbraco.TypedContentAtRoot().DescendantsOrSelf("home").ToList();
            var documentTypes = SetDocumentTypes();
            SetPageIndex(rootNode, documentTypes, pages);

            var resetIndex = new ResetSolrIndex();

            var url = WebConfigurationManager.AppSettings["solrUrl"];

            resetIndex.ResetIndex(url, pages);
            var redirectUrl = $"/resetindex";

            return Redirect(redirectUrl);
        }

        private List<string> SetDocumentTypes()
        {
            return new List<string>
            {
                "home",
                "blog",
                "portfolio"
            };
        }

        private void SetPageIndex(IEnumerable<IPublishedContent> rootNode, List<string> documentTypes, List<Page> pages)
        {

            foreach (var node in rootNode)
            {
                if (documentTypes.Contains(node.DocumentTypeAlias))
                {
                    var page = new Page
                    {
                        Content = node.GetProperty("bodyText").DataValue.ToString(),
                        Id = node.Id.ToString(),
                        PageTitle = node.GetProperty("pageTitle").DataValue.ToString(),
                        //PageDescription = node.GetProperty("pageDescription").DataValue.ToString()
                    };
                    pages.Add(page);

                    if (node.Children.Any())
                    {
                        SetPageIndex(node.Children, documentTypes, pages);
                    }
                }

            }
        }
    }
}