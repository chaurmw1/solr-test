﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using SearchLibrary;
using Umbraco.Web;

[assembly: OwinStartup(typeof(SolrShowcase.Startup))]

namespace SolrShowcase
{
    public class Startup : UmbracoDefaultOwinStartup
    {
        public void Configuration(IAppBuilder app)
        {
            Connection.InitializeSolrConnection(ConfigurationManager.AppSettings["solrUrl"]);
        }
    }
}
